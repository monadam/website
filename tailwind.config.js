const colors = require("tailwindcss/colors");
/** @type {import('tailwindcss').Config} */
export default {
  separator: "_",
  theme: {
    extend: {
      fontFamily: { title: ['"Comfortaa"'] },
      colors: {
        debug: "magenta",
        primary: colors.sky,
        secondary: colors.emerald,
      },
    },
  },
  plugins: [],
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./app.vue",
    "./error.vue",
  ],
};
