export default defineNuxtConfig({
  app: {
    rootId: "app",
  },
  devtools: { enabled: false },

  modules: ["@nuxt/content"],

  content: {
    documentDriven: true,
    markdown: {
      remarkPlugins: ["remark-math"],
      rehypePlugins: ["rehype-mathjax"],
    },
  },

  css: ["~/assets/main.scss"],

  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },

  vite: {
    vue: {},
  },
});
