export type F = (x: number) => number;

export interface Point {
  x: number;
  y: number;
}

export interface Line {
  from: Point;
  to: Point;
}

export interface Rect extends Point {
  w: number;
  h: number;
}

export interface DrawOptions {
  color?: string;
  fill?: boolean;
  size?: number;
  radius?: number;
}

export const fs: F[] = [
  (x) => Math.pow(x, 3) - 2,
  (x) => Math.pow(x, 2) - 3,
  (x) => 1 / (1 + x * x) - 0.5,
  (x) => Math.exp(x) - 1,
  (x) => Math.sin(x),
];

class Camera {
  cx: number;
  cy: number;
  constructor(
    public x: number,
    public y: number,
    public w: number,
    public h: number,
  ) {
    this.cx = x + w / 2;
    this.cy = y + h / 2;
  }
  move(x: number, y: number) {
    this.x += x;
    this.y += y;
  }
  zoom(scale: number) {
    this.w = this.w * scale;
    this.h = this.h * scale;
    this.x = this.x + ((this.w / 2) * (1 - scale)) / 2;
    this.y = this.y + ((this.h / 2) * (1 - scale)) / 2;
  }
}

export class CanvasManager {
  ctx: CanvasRenderingContext2D;
  canvas: HTMLCanvasElement;
  camera: Camera;
  options: { [key: string]: DrawOptions } = {
    default: {
      color: "white",
      size: 2,
    },
    f: { color: "cyan", size: 2 },
    axes: { color: "gray" },
    point: { color: "white" },
    root: { color: "red", size: 2, fill: true },
  };

  constructor(canvas: HTMLCanvasElement) {
    this.canvas = canvas;
    this.ctx = canvas.getContext("2d")!;

    canvas.width = canvas.clientWidth;
    canvas.height = canvas.clientHeight;

    this.camera = new Camera(-1, 2, 5, 5);
  }

  get scale(): Point {
    return {
      x: this.canvas.width / this.camera.w,
      y: this.canvas.height / this.camera.h,
    };
  }

  get center(): Point {
    return {
      x: this.camera.cx,
      y: this.camera.cy,
    };
  }

  translate({ x, y }: Point): Point {
    return {
      x: (x - this.camera.x) * this.scale.x,
      y: (-1 * y + this.camera.y) * this.scale.y,
    };
  }

  loadOptions({ color, size }: DrawOptions) {
    this.ctx.fillStyle = color || this.options.default.color!;
    this.ctx.strokeStyle = color || this.options.default.color!;
    this.ctx.lineWidth = size || this.options.default.size!;
  }

  debug() {}

  drawCircle(p: Point, options?: DrawOptions) {
    let radius: number;
    if (options) {
      this.loadOptions(options);
    }
    radius = options?.radius || 3;
    const np = this.translate(p);
    this.ctx.moveTo(np.x, np.y);
    this.ctx.beginPath();
    this.ctx.arc(np.x, np.y, radius, 0, Math.PI * 2);
    if (options?.fill) this.ctx.fill();
    else this.ctx.stroke();
  }

  clear() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.debug(); //remove
  }

  drawLine({ from, to }: Line, options?: DrawOptions) {
    this.drawLines([{ from, to }], options);
  }

  drawLines(lines: Line[], options?: DrawOptions) {
    if (options) this.loadOptions(options);
    this.ctx.beginPath();
    for (let { from, to } of lines) {
      const nf = this.translate(from);
      const nt = this.translate(to);
      this.ctx.moveTo(nf.x, nf.y);
      this.ctx.lineTo(nt.x, nt.y);
    }
    this.ctx.stroke();
  }

  drawAxes(options?: DrawOptions) {
    const xAxis: Line = {
      from: { x: -5, y: 0 },
      to: { x: 5, y: 0 },
    };
    const yAxis: Line = {
      from: { y: -5, x: 0 },
      to: { y: 5, x: 0 },
    };

    const xTicks: Point[] = new Array(10)
      .fill(0)
      .map((_, i) => i)
      .map((i) => {
        return this.translate({
          x: 0,
          y: 5 - i,
        });
      });
    const yTicks: Point[] = new Array(10)
      .fill(0)
      .map((_, i) => i)
      .map((i) => {
        return this.translate({
          x: 5 - i,
          y: 0,
        });
      });
    this.drawLines([xAxis, yAxis], { ...this.options.axes, ...options });

    [...xTicks, ...yTicks].forEach((tick) => {
      this.drawCircle(tick);
    });
  }

  plotF(f: F, xs: number[] = []) {
    const pts: Point[] = new Array(100).fill(0).map((_, i) => {
      const x: number = (50 - i) / 10;
      const y: number = f(x);
      return { x, y };
    });
    for (let i = 1; i < pts.length; i++) {
      this.drawLine(
        {
          from: pts[i - 1],
          to: pts[i],
        },
        this.options.f,
      );
    }
  }
}
